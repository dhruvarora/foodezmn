from django.db import models

# Create your models here.
class PinCodes(models.Model):
    area_name = models.CharField(max_length=150)
    city = models.CharField(max_length=150, default="Gurgaon")
    pincode = models.IntegerField(default=122001)

    def __str__(self):
        return self.area_name + " " + str(self.pincode)
