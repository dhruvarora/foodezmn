from django.shortcuts import render
import json
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.core.serializers import serialize
from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404
from django.contrib.auth import login as django_login

from saleor.product.models import Category, Product, ProductVariant
from saleor.order.models import *
from saleor.userprofile.models import *
from saleor.mobile.models import PinCodes

@csrf_exempt
def signup(request):
    data = json.loads(request.body)
    email = data["email"]
    password = data["password"]
    user = authenticate(request=request, email=email, password=password)
    if user is not None:
        return HttpResponse("false")
    else:
        User.objects.create_user(email=email, password=password)
        return HttpResponse("true")

@csrf_exempt
def login(request):
    if request.method == "POST":
        print (request.body)
        data = json.loads(request.body)
        username = data["username"]
        password = data["password"]
        user = authenticate(request=request, email=username, password=password)
        status = 200
        ctx = {}
        if user is not None and user.is_active:
            django_login(request, user)
            ctx = {"status":"true"}
        else:
            status = 400
            ctx = {"status":"false"}
        return JsonResponse(ctx, status=status)

@csrf_exempt
def AllCategories(request):
    categories = Category.objects.all()
    data = serialize("json", categories)

    return JsonResponse(data, safe=False)

@csrf_exempt
def get_category(request):
    category_id = request.GET.get("cid")
    category = get_object_or_404(Category, id=category_id)
    if category:
        return HttpResponse(category)
    else:
        return HttpResponse("404")

@csrf_exempt
def recent_orders(request):
    data = json.loads(request.body)
    email = data["email"]
    token = data["token"]
    orders = Order.objects.filter(user_email=email, token=token)

    data = serialize("json", orders)

    return JsonResponse(data, safe=False)

@csrf_exempt
def get_product_category_wise(request):
    category_id = request.GET.get("category_id")
    category = get_object_or_404(Category, id=category_id)
    products = Product.objects.filter(categories__id=category.id)

    data = serialize("json", products)

    return JsonResponse(data, safe=False)

@csrf_exempt
def get_product(request):
    product_id = request.GET.get("product_id")
    product = Product.objects.get(id=product_id)
    variants = ProductVariant.objects.filter(product=product)

    data = serialize("json", variants)

    return JsonResponse(data, safe=False)

@csrf_exempt
def pincheck(request):
    if request.POST.get("pincode"):
        pincode = int(request.POST.get("pincode"))
    else:
        data = json.loads(request.body)
        pincode = int(data["pincode"])
    try:
        status = PinCodes.objects.get(pincode=pincode)
    except:
        return HttpResponse("false")
    else:
        return HttpResponse("true")

@csrf_exempt
def AddShipmentAddress(request):
    try:
        data = request.body
        new_data = data.decode("utf-8", "strict")
        data = json.loads(new_data)
        # curl -i -X; POST -H; "Content-Type:application/json" http://localhost:8000/demo-rest-jersey-spring/podcasts/ -d '{"first_name":"yoyo","last_name":"honey","company_name":"xane","street_address_1":"112","street_address_2":"sector6","city":"noida","city_area":"alkjfds","postal_code":"233220","country":"india","country_area":"delhi","phone":"7838382748"}'
        first_name = data["first_name"]
        last_name = data["last_name"]
        company_name = data["company_name"]
        street_address_1 = data["street_address_1"]
        street_address_2 = data["street_address_2"]
        city = data["city"]
        city_area = data["city_area"]
        postal_code = data["postal_code"]
        country = data["country"]
        country_area = data["country_area"]
        phone = data["phone"]

        # address =  Address(first_name=first_name,last_name =last_name,company_name=company_name,street_address_1=street_address_1,street_address_2=street_address_2,city=city,city_area=city_area,postal_code=postal_code,country=country,country_area=country_area,phone=phone)
        address =  Address(first_name=first_name,last_name =last_name,company_name=company_name,street_address_1=street_address_1,street_address_2=street_address_2,city=city,city_area=city_area,postal_code=postal_code,country_area=country_area,phone=phone)
        address.save()
        id = address.id
        data = {'status':True,'id':id}
        return HttpResponse(data)
    except Exception as e:
        print (e)
        return HttpResponse(e)

# status order shipping_price shipping_method_name tracking_number last_updated
@csrf_exempt
def pointAdressToUser(request):
    try:
        data = request.body
        new_data = data.decode("utf-8", "strict")
        data = json.loads(new_data)
        addressID = int(data["addressId"])
        userId = int(data["userId"])
        address = Address.objects.get(id=addressID)
        user = User.objects.get(id=userId)
        user.addresses.add(address)
        user.save()

        if data:
            print("Success..........")
            return HttpResponse("True")
        else:
            return HttpResponse("false")
    except Exception as e:
        print(e)
        return HttpResponse(e)


@csrf_exempt
def AddDeliveryGroup(request):
    try:
        data = request.body
        new_data = data.decode("utf-8", "strict")
        data = json.loads(new_data)
        order = data["order"]
        shipping_price = data["shipping_price"]
        shipping_method_name = data["shipping_method_name"]
        tracking_number = data["tracking_number"]

        deliverygroup = DeliveryGroup(order=int(order), shipping_price=shipping_price, shipping_method_name=shipping_method_name,tracking_number=tracking_number)
        # deliverygroup = DeliveryGroup(shipping_price=shipping_price, shipping_method_name=shipping_method_name,tracking_number=tracking_number)
        deliverygroup.save()
        if data:
            print("Success..........")
            return HttpResponse("True")
        else:
            return HttpResponse("false")
            return HttpResponse("false")
    except Exception as e:
        print(e)
        return HttpResponse(e)

@csrf_exempt
def AddOrderDetails(request):
    try:
        data = request.body
        new_data = data.decode("utf-8", "strict")
        data = json.loads(new_data)
        order_number = data["order_number"]
        user = data["user_id"]
        billing_address = data["billing_address_id"]
        shipping_address = data["shipping_address_id"]
        user_email = data["user_email"]
        discount_amount = data["discount_amount"]
        discount_name = data["discount_name"]
        voucher = data["voucher"]

        order = Order(order_number=order_number, user = int(user), billing_address =int(billing_address),  shipping_address = int(shipping_address), user_email = user_email, discount_amount = discount_amount, discount_name = discount_name, voucher=voucher)
        dd = order.save()
        print ("this has to printed",dd)
        if data:
            print("Success..........")
            return HttpResponse("True")
        else:
            return HttpResponse("false")
    except Exception as e:
        print(e)
        return HttpResponse(e)


@csrf_exempt
def CreateOrder(request):
    try:
        data = json.loads(request.body)
        delivery_group_id = data["delivery_group_id"]
        product_id = data["product_id"]
        product_name = data["product_name"]
        quantity = data["quantity"]
        rate = data["rate"]
        user_id = data["user_id"]
        order = data["order_id"]
        unit_price_gross = data["unit_price_gross"]

        order = OrderLine(delivery_group_id=int(delivery_group_id), product=int(product_id), product_name=product_name, quantity=quantity, unit_price_net=rate,unit_price_gross=unit_price_gross)
        # order = OrderLine(delivery_group_id=int(delivery_group_id),product_name=product_name, quantity=quantity, unit_price_net=rate,unit_price_gross=unit_price_gross)
        order.save()
        order_note = OrderNote(user=int(user_id), order=int(order))
        order_note.save()
        if data:
            print("Success..........")
            return HttpResponse("True")
        else:
            return HttpResponse("false")
    except Exception as e:
        print(e)
        return HttpResponse(e)
