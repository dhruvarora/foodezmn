from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.login),
    url(r'^signup/$', views.signup),
    url(r'^all-categories/$', views.AllCategories),
    url(r'^category/$', views.get_category),
    url(r'^recent-orders/$', views.recent_orders),
    url(r'^get-product-category-wise/$', views.get_product_category_wise),
    url(r'^get-product/$', views.get_product),
    url(r'^pincheck/', views.pincheck),
    url(r'^create-order/', views.CreateOrder),
    url(r'^add-shipmentAddress/', views.AddShipmentAddress),
    url(r'^add-deliveryGroup/', views.AddDeliveryGroup),
    url(r'^add-orderDetails/', views.AddOrderDetails),
    url(r'^point-adressto-user/', views.pointAdressToUser),
]
