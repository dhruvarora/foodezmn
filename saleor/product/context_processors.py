from .models import ProductVariant
from .models import Product

def product_returns(request):
    ep = ProductVariant.objects.filter(product__exclusive=True, product__is_published=True)
    featured = ProductVariant.objects.filter(product__is_featured=True, product__is_published=True)
    ctx = {
    "exclusive_products":ep,
    "featured":featured,
    }
    return ctx
