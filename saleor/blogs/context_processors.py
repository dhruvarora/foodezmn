from .models import Blog, Category
from django.shortcuts import get_object_or_404 as fetch

def return_recipes(request):
    # category = fetch(Category, name="Recipes")
    blogs = Blog.objects.all().order_by("-date_added")
    ctx = {"blogs":blogs}

    return ctx
