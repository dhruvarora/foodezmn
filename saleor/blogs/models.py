from django.db import models
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField

class Category(models.Model):
    name = models.CharField(max_length=150)

    def __str__(self):
        return self.name

class Blog(models.Model):
    date_added = models.DateTimeField(default=timezone.now)
    title = models.CharField(max_length=150)
    categories = models.ManyToManyField(Category)
    author = models.CharField(max_length=150)
    introText = models.TextField(blank=True, null=True)
    introImage = models.ImageField(default=True, null=True)
    content = RichTextUploadingField(blank=True, null=True)

    def __str__(self):
        return self.title
